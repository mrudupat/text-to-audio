import { Component, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent {
  utterance: string = '';
  selectedLanguage: string = 'hindi';

  @Output() utteranceSubmitted: EventEmitter<{ language: string, utterance: string }> = new EventEmitter();
  constructor(private http: HttpClient) {}
  submitUtterance(): void {
    if (this.utterance && this.selectedLanguage) {
      this.utteranceSubmitted.emit({ language: this.selectedLanguage, utterance: this.utterance });
    }

    this.http.post<any>('http://localhost:3000/api/utterances', { language: this.selectedLanguage, utterance: this.utterance })
      .subscribe(
        response => {
          console.log(response);
          // Handle the response as needed
        },
        error => {
          console.error(error);
          // Handle the error as needed
        }
      );

    this.utterance = ''; // Move this line here to reset the utterance after the HTTP POST request
  }

}
