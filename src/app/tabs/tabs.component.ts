import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css'],
})
export class TabsComponent {
  @Input() utterancesByLanguage: { [key: string]: string[] } = {};
  selectedLanguage: string = 'hindi';
  languages: string[] = ['hindi', 'english']; // Add more languages here
  utterance: string = '';

  playAudio(text: string): void {
    if (text.trim() != '') {
      const utterance = text.replace(/[0-9a-f]{64}\s*/, ''); // Remove the hashcode from the text
     // const utteranceOnly = utterance.trim(); // Remove leading/trailing whitespaces from the utterance

      // Rest of the code remains the same
      console.log(utterance);
      const utteranceObj = new SpeechSynthesisUtterance();
      utteranceObj.text = utterance;

      const voices = speechSynthesis.getVoices();
      utteranceObj.voice = voices[0]; // Replace with the desired voice

      speechSynthesis.speak(utteranceObj);
    }
  }


}
